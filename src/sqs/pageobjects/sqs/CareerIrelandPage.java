package sqs.pageobjects.sqs;

import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.Label;

import org.openqa.selenium.By;

import sqs.common.PageObject;

/**
 * The Career Ireland Page Object
 * <img src='doc-files/CareerIrelandPage.jpg' alt=''>
 */
public class CareerIrelandPage extends PageObject {

  private final Hyperlink
      vacanciesAtSqs =
      new Hyperlink(By.xpath("//a[text()='Vacancies at SQS']"));
  private final Label
      growYourCareerWithSqsIreland =
      new Label(By.xpath("//h1[text()='Grow your career with SQS Ireland']"));

  /**
   * Instantiates a new Sqs career portal page
   */
  public CareerIrelandPage() {
  }

  public void onPage() {
    growYourCareerWithSqsIreland.isPresent();
  }

  public VacanciesPage navigateToVacanciesPage() {
    vacanciesAtSqs.click();
    return navigatingTo(VacanciesPage.class);
  }


}
