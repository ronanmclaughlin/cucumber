package sqs.pageobjects.sqs;


import com.sqs.web.elements.Hyperlink;
import com.sqs.web.webdriver.DriverProvider;

import org.openqa.selenium.By;

import lombok.Getter;
import sqs.common.PageObject;

/**
 * The SQS Home Page Object
 * <img src='doc-files/SqsHomePage.jpg' alt=''>
 */
public class SqsHomePage extends PageObject {

  @Getter
  private final String URL = "http://www.sqs.com";
  @Getter
  private final Hyperlink sqsLogo = new Hyperlink(By.xpath("//h2[.='Specialist Consultancy']"));
  protected TopNavigationBar navBar; // this is composition.

  /**
   * Instantiates a new Sqs home page.
   */
  public SqsHomePage() {
    this.navBar = new TopNavigationBar(); // instantiate composite object in the constructor
  }

  public void onPage() {
    sqsLogo.isDisplayed();
  }

  /**
   * Navigate to page object.
   *
   * @return the page object
   */
  public SqsHomePage init() {
    DriverProvider.getDriver().get(URL);
    return navigatingTo(SqsHomePage.class);
  }

  /**
   * Navigate to page object.
   *
   * @return the page object
   */
  public SqsCareerPortalPage navigateToCareersPage() {
    return navBar.navigateToCareers();
  }

}
