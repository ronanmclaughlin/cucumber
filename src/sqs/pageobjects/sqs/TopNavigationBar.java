package sqs.pageobjects.sqs;


import com.sqs.web.elements.Hyperlink;

import org.openqa.selenium.By;

import lombok.Getter;
import sqs.common.PageObject;

/**
 * The Top Navigation Page Object
 * <img src='doc-files/TopNavigationBar.jpg' alt=''>
 */
public class TopNavigationBar extends PageObject {

  @Getter
  private final Hyperlink career = new Hyperlink(By.xpath("//a[.='Career']"));

  /**
   * Instantiates a new Navigation bar.
   */
  public TopNavigationBar() {
  }

  public void onPage() {
    career.isDisplayed();
  }

  /**
   * Navigate to page object.
   *
   * @return the page object
   */
  // method that hides the nav bar functionality internally
  public SqsCareerPortalPage navigateToCareers() {
    career.click();
    return navigatingTo(SqsCareerPortalPage.class);
  }


  /**
   * List of pages on navbar.
   * using enums can restrict user input,
   * therefore avoiding user input errors later
   */
  public enum Pages {
    /**
     * Newsroom
     */
    NEWSROOM,
    /**
     * Training
     */
    TRAINING,
    /**
     * Investors
     */
    INVESTORS,
    /**
     * Careers
     */
    CAREER,
    /**
     * Countries
     */
    COUNTRIES,
    /**
     * DE
     */
    DE
  }
}
