package sqs.pageobjects.sqs;


import com.sqs.web.elements.Hyperlink;

import org.openqa.selenium.By;

import sqs.common.PageObject;

/**
 * The Career Portal Page Object
 * <img src='doc-files/SqsCareerPortalPage.jpg' alt=''>
 */
public class SqsCareerPortalPage extends PageObject {

  private final Hyperlink ireland = new Hyperlink(By.xpath("//a[.='Ireland']"));
  public final Hyperlink graduates = new Hyperlink(By.linkText("Graduates"));
  public final Hyperlink professionals = new Hyperlink(By.linkText("Professionals"));

  /**
   * Instantiates a new Sqs career portal page
   */
  public SqsCareerPortalPage() {
  }

  public void onPage() {
    ireland.isPresent();
  }

}
