package sqs.pageobjects.sqs;


import com.sqs.web.elements.Button;
import com.sqs.web.elements.Frame;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.Table;

import org.openqa.selenium.By;

import lombok.Getter;
import sqs.common.PageObject;

/**
 * The Vacancies Page Object
 * <img src='doc-files/VacanciesPage.jpg' alt=''>
 */
public class VacanciesPage extends PageObject {

  private final Label jobListings = new Label(By.xpath("//h2[Contains(text(),'Job Listings')]"));
  private final Button searchButton = new Button(By.xpath("//input[@value='Search']"));
  @Getter
  private final Table
      vacanciesTable =
      new Table(By.xpath("//table[@class='iCIMS_JobsTable iCIMS_Table']"));
  private final Frame frame = new Frame(By.xpath("//iframe[@id='icims_content_iframe']"));

  /**
   * Instantiates a new Sqs career portal page
   */
  public VacanciesPage() {
  }

  public void onPage() {
    frame.switchToFrame();
    searchButton.isDisplayed();
  }

  public VacanciesPage searchForVacancies() {
    searchButton.click();
    return this;
  }

}
