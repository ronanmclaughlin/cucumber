package sqs.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "resources/features/Checking Careers Page.feature",
    glue = "sqs.stepfiles",
    format = {"pretty"})
public class FeatureRunner extends AbstractTestNGCucumberTests {

}