package sqs.common;


import com.sqs.core.sqslibs.WebLog;

/**
 * Base page object
 */
public abstract class PageObject {

  /**
   * method that instantiates a new page object
   * and verifies the page has loaded
   *
   * @param c   the page object class to instantiate
   * @param <D> the page object class to instantiate
   * @return the page object class
   */
  public <D extends PageObject> D navigatingTo(Class<D> c) {
    D newPage = null;
    try {
      WebLog.info("Navigating to page: '" + c.getSimpleName() + "'");
      newPage = c.newInstance();
      //Ensure we are on the correct page
      newPage.onPage();
    } catch (Exception e) {
      WebLog.info("Unable to navigate to page: '" + c.getSimpleName() + "' due to exception: " + e
          .toString());
    }
    return newPage;
  }

  public abstract void onPage();


}
