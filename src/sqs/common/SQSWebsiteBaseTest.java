/*Copyright 2014 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
*/

package sqs.common;

import com.sqs.core.sqslibs.WebLog;

import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;


/**
 * The type Test NG base test.
 */
public class SQSWebsiteBaseTest {

  /**
   * Sets up test. Stores test name for logging purposes.
   *
   * @param browser the browser
   * @param context the context for the test case
   */
  @Parameters("browser")
  @BeforeTest
  protected void setupTest(@Optional("firefox") String browser, ITestContext context) {
    WebLog.startTestCase(context.getName());
  }

  /**
   * Tear down.
   *
   * @param context the context for the test case
   */
  @AfterTest
  protected void tearDownTest(ITestContext context) {
    WebLog.endTestCase(context.getName());
  }

}
