package sqs.stepfiles;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import sqs.pageobjects.sqs.SqsCareerPortalPage;
import sqs.pageobjects.sqs.SqsHomePage;

/**
 * This is the test class for Cucumber that runs the stories as JUnit tests
 * using the Cucumber JUnit runner.
 */
@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber"},
    features = "src/test/resources/Stories/Cucumber/")
public class ParentScenario {

  protected SqsHomePage sqsHomePage;
  protected SqsCareerPortalPage sqsCareerPortalPage;
  protected String url = "https://www.sqs.com/uk/index.php";

}
