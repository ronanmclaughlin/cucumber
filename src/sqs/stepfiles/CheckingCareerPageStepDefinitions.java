package sqs.stepfiles;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static ru.yandex.qatools.matchers.webdriver.DisplayedMatcher.displayed;

import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.webdriver.DriverProvider;

import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by mclaughlir on 13/09/2016.
 */
public class CheckingCareerPageStepDefinitions extends ParentScenario {


  /**
   * Sets up test. Stores test name for logging purposes.
   *
   * @param browser the browser
   * @param context the context for the test case
   */
  @Parameters("browser")
  @BeforeTest
  protected void setupTest(@Optional("firefox") String browser, ITestContext context) {
    WebLog.startTestCase(context.getName());
  }

  /**
   * Tear down.
   *
   * @param context the context for the test case
   */
  @AfterTest
  protected void tearDownTest(ITestContext context) {
    WebLog.endTestCase(context.getName());
  }

  @Given("^I am on the \"([^\"]*)\" page$")
  public void iAmOnThePage(String pageTitle) throws Throwable {
    if (pageTitle.equalsIgnoreCase("sqs home")) {
      DriverProvider.getDriver().get(url);
    }
  }

  @When("^I click the \"([^\"]*)\" link$")
  public void iClickTheLink(String link) throws Throwable {
    if ("career".equalsIgnoreCase(link)) {
      sqsHomePage.navigateToCareersPage();
    }
  }

  @Then("^I should see the \"([^\"]*)\" link on Career page$")
  public void iShouldSeeTheLinkOnCareerPage(String link) throws Throwable {

    if ("graduates".equalsIgnoreCase(link)) {
      assertThat(sqsCareerPortalPage.graduates, is(displayed()));
    }

    if ("professionals".equalsIgnoreCase(link)) {
      assertThat(sqsCareerPortalPage.professionals, is(displayed()));
    }
  }
}
