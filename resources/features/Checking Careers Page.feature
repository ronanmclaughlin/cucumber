Feature: Checking Careers page
    In order to view careers with SQS, I want to get to the Careers page

    Scenario: Navigating to career page
        Given I am on the "SQS home" page
        When I click the "Career" link
        Then I should see the "Graduates" link on Career page
        And I should see the "Professionals" link on Career page